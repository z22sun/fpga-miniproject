-------------------------------------------------------------------------------
-- Title      : controlUnit
-- Project    :
-------------------------------------------------------------------------------
-- File       : operativeUnit.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-04-11
-- Last update: 2019-02-13
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Control unit of a sequential FIR filter.
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-04-11  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity controlUnit_echo is

  port (
    I_clock               : in  std_logic;  -- global clock
    I_reset               : in  std_logic;  -- asynchronous global reset
    I_inputSampleValid    : in  std_logic;  -- Control signal to load the input sample
    O_incrAddress         : out std_logic;  -- Control signal to increment register read addres
    O_loadY               : out std_logic  -- Control signal to load Y register
    );

end entity controlUnit_echo;
architecture archi_operativeUnit_echo of controlUnit_echo is


  type T_state is (WAIT_SAMPLE, STORE, OUTPUT, WAIT_END_SAMPLE);  -- state list
  signal SR_presentState : T_state;
  signal SR_futurState   : T_state;

begin

  process (I_clock, I_reset) is
  begin
    if I_reset = '1' then               -- asynchronous reset (active high)
      SR_presentState <= WAIT_SAMPLE;
    elsif rising_edge(I_clock) then     -- rising clock edge
      SR_presentState <= SR_futurState;
    end if;
  end process;

  process (I_inputSampleValid, SR_presentState) is
  begin
    case SR_presentState is

      when WAIT_SAMPLE =>
        SR_futurState <= WAIT_SAMPLE;
        if (I_inputSampleValid = '1') then
            SR_futurState <= STORE;
        end if;
        
      when STORE =>
        SR_futurState <= OUTPUT;
        
      when OUTPUT =>
        SR_futurState <= WAIT_END_SAMPLE;
        
      when WAIT_END_SAMPLE =>
        SR_futurState <= WAIT_END_SAMPLE;
        if (I_inputSampleValid = '0') then
            SR_futurState <= WAIT_SAMPLE;
        end if;
        
      when others => null;
    end case;
  end process;

  O_incrAddress         <= '1' when (SR_presentState = STORE) else '0';
  O_loadY               <= '1' when (SR_presentState = OUTPUT) else '0';

end architecture archi_operativeUnit_echo;
