-------------------------------------------------------------------------------
-- Title      : firUnit
-- Project    : 
-------------------------------------------------------------------------------
-- File       : operativeUnit.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    : 
-- Created    : 2018-04-11
-- Last update: 2018-04-11
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 8 bit FIR
-------------------------------------------------------------------------------
-- Copyright (c) 2018 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-04-11  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity firUnit is

  port (
    I_clock               : in  std_logic;  -- global clock
    I_reset               : in  std_logic;  -- asynchronous global reset
    I_inputSample         : in  std_logic_vector(7 downto 0);  -- 8 bit input sample
    I_inputSampleValid    : in  std_logic;
    O_filteredSample      : out std_logic_vector(7 downto 0);  -- filtered sample
    O_filteredSampleValid : out std_logic
    );

end entity firUnit;

architecture archi_firUnit of firUnit is

  signal SC_processingDone      : std_logic;
  signal SC_incrAddressFilter         : std_logic;
  signal SC_incrAddressEcho         : std_logic;
  signal SC_loadYFilter               : std_logic;
  signal SC_loadYEcho               : std_logic;
  signal SC_initAddress         : std_logic;
  signal SC_initSum             : std_logic;
  signal SC_loadSum             : std_logic;
  signal SC_loadShift           : std_logic;
  signal SC_filteredSample      : std_logic_vector(7 downto 0);
  signal SC_filteredSampleValid : std_logic;

begin
    
  O_filteredSampleValid <= SC_filteredSampleValid;

  controlUnit_filter : entity work.controlUnit_filter
    port map (
      I_clock               => I_clock,
      I_reset               => I_reset,
      I_inputSampleValid    => I_inputSampleValid,
      I_processingDone      => SC_processingDone,
      O_loadShift           => SC_loadShift,
      O_initAddress         => SC_initAddress,
      O_incrAddress         => SC_incrAddressFilter,
      O_initSum             => SC_initSum,
      O_loadSum             => SC_loadSum,
      O_loadY               => SC_loadYFilter,
      O_FilteredSampleValid => SC_filteredSampleValid
      );

  operativeUnit_filter : entity work.operativeUnit_filter
    port map (
      I_clock          => I_clock,
      I_reset          => I_reset,
      I_inputSample    => I_inputSample,
      I_loadShift      => SC_loadShift,
      I_initAddress    => SC_initAddress,
      I_incrAddress    => SC_incrAddressFilter,
      I_initSum        => SC_initSum,
      I_loadSum        => SC_loadSum,
      I_loadY          => SC_loadYFilter,
      O_processingDone => SC_processingDone,
      O_Y              => SC_filteredSample
      );


  controlUnit_echo : entity work.controlUnit_echo
    port map (
      I_clock               => I_clock,
      I_reset               => I_reset,
      I_inputSampleValid    => SC_filteredSampleValid,
      O_incrAddress         => SC_incrAddressEcho,
      O_loadY               => SC_loadYEcho);

  operativeUnit_echo : entity work.operativeUnit_echo
    port map (
      I_clock               => I_clock,
      I_reset               => I_reset,
      I_filteredInputSample => SC_filteredSample,
      I_inputSample         => I_inputSample,
      I_incrAddress         => SC_incrAddressEcho,
      I_loadY               => SC_loadYEcho,
      O_Y                   => O_filteredSample);

end architecture archi_firUnit;
