-------------------------------------------------------------------------------
-- Title      : operativeUnit
-- Project    :
-------------------------------------------------------------------------------
-- File       : operativeUnit.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-04-11
-- Last update: 2019-02-13
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Operative unit of a sequential FIR filter. Including shift
-- register for samples, registers for coefficients, a MAC and a register to
-- store the result
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-02-13  1.1      marzel  Update to provide a 16-tap filter and improve
--                              the user experience ;)
-- 2018-04-11  1.0      jnbazin Created
-- 2018-04-18  1.0      marzel  Modification of SR_Y assignment to a round
--                              instead of a trunc
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity operativeUnit_echo is

  port (
    I_clock                 : in  std_logic;   -- global clock
    I_reset                 : in  std_logic;   -- asynchronous global reset
    I_inputSample           : in  std_logic_vector(7 downto 0);  -- 8 bit input sample
    I_filteredInputSample   : in  std_logic_vector(7 downto 0);  -- 8 bit input sample
    I_incrAddress           : in  std_logic;  -- Control signal to increment register read address
    I_loadY                 : in  std_logic;   -- Control signal to load Y register
    O_Y                     : out std_logic_vector(7 downto 0)   -- filtered sample
    );

end entity operativeUnit_echo;

architecture arch_operativeUnit of operativeUnit_echo is
  
  
  constant SAMPLE_RATE: integer := 48000; -- samples/second
  constant ECHOES_AMOUNT: integer := 2; -- number of echoes
  
  type bufferType is array(0 to SAMPLE_RATE - 1) of signed(7 downto 0);
  
  signal soundBuffer: bufferType := (others => (others => '0'));
  
  signal bufferWriteAddress : integer range 0 to SAMPLE_RATE - 1 := 0;
  --signal bufferReadAddress : integer range 0 to SAMPLE_RATE - 1 := 0;
  
  --signal echoSample: std_logic_vector(7 downto 0) := (others => '0');
  type addressesList is array (0 to ECHOES_AMOUNT - 1) of integer range 0 to SAMPLE_RATE - 1;
  signal bufferReadAddresses : addressesList := (others => 0);
  
  type echoList is array (0 to ECHOES_AMOUNT - 1) of std_logic_vector(7 downto 0);
  
  signal echoSamples : echoList := (others => (others => '0'));
  signal currentSample: std_logic_vector(7 downto 0) := (others => '0');
  
  signal SR_Y: signed(8 downto 0);

begin

--  increment_address: process (I_clock, I_reset) is
--  begin
--    if I_reset = '1' then               -- asynchronous reset (active high)
--      bufferWriteAddress <= 0;
--      bufferReadAddress <= 1;
--    elsif RISING_EDGE(I_clock) then 
--        if (I_incrAddress = '1') then
        
--            bufferWriteAddress <= bufferWriteAddress + 1;
--            bufferReadAddress <= bufferReadAddress + 1;
            
--            if (bufferWriteAddress >= SAMPLE_RATE - 1) then
--                bufferWriteAddress <= 0;
--            end if;
--            if (bufferReadAddress >= SAMPLE_RATE - 1) then
--                bufferReadAddress <= 0;
--            end if;
--        end if;
--    end if;
--  end process;


  increment_address: process (I_clock, I_reset) is
  begin
    if I_reset = '1' then               -- asynchronous reset (active high)
      bufferWriteAddress <= 0;
      bufferReadAddresses <= (others => 1);
      for i in 0 to ECHOES_AMOUNT - 1 loop
        bufferReadAddresses(i) <= 48000 - 48000 / (i + 1) + 1;
      end loop;
      
    elsif RISING_EDGE(I_clock) then 
        if (I_incrAddress = '1') then
            bufferWriteAddress <= bufferWriteAddress + 1;
            if (bufferWriteAddress >= SAMPLE_RATE - 1) then
                bufferWriteAddress <= 0;
            end if;
            
            for i in 0 to ECHOES_AMOUNT - 1 loop
                bufferReadAddresses(i) <= bufferReadAddresses(i) + 1;
                if (bufferReadAddresses(i) >= SAMPLE_RATE - 1) then
                    bufferReadAddresses(i) <= 0;
                end if;
            end loop; 
        end if;
    end if;
  end process;
  
  processing: process (I_clock, I_reset) is
  begin
    if I_reset = '1' then               -- asynchronous reset (active high)
      echoSamples <= (others => (others => '0'));
    elsif RISING_EDGE(I_clock) then 
        if (I_incrAddress = '1') then  
            soundBuffer(bufferWriteAddress) <= signed(I_filteredInputSample);
            
            for i in 0 to ECHOES_AMOUNT - 1 loop
                echoSamples(i) <= std_logic_vector(soundBuffer(bufferReadAddresses(i)));
            end loop;
            
            currentSample <= I_inputSample; -- we might use the sample stored in soundBuffer
        end if;
    end if;
  end process;
  
--  processing: process (I_clock, I_reset) is
--  begin
--    if I_reset = '1' then               -- asynchronous reset (active high)
--      echoSample <= (others => '0');
--    elsif RISING_EDGE(I_clock) then 
--        if (I_incrAddress = '1') then  
--            soundBuffer(bufferWriteAddress) <= signed(I_filteredInputSample);
--            echoSample <= std_logic_vector(soundBuffer(bufferReadAddress));
--            currentSample <= I_inputSample; -- we might use the sample stored in soundBuffer
--        end if;
--    end if;
--  end process;

  store_result : process (I_reset, I_clock) is
  variable accumulator : signed(8 downto 0) := (others => '0');
  begin
    if RISING_EDGE(I_clock) then
        if (I_loadY  = '1') then
            
            for i in 0 to ECHOES_AMOUNT - 1 loop
                accumulator := resize(shift_right(signed(echoSamples(i)), i + 1), 9);
            end loop;
        
        
            SR_Y <= resize(signed(currentSample), 9) + accumulator;
            -- SR_Y <= resize(signed(currentSample), 9) + resize(shift_right(signed(echoSample), 1), 9);
        end if;
      end if;
  end process store_result;

  O_Y <= std_logic_vector(signed(SR_Y(7 downto 0)));

end architecture arch_operativeUnit;
